<?php
/**
 * The template for displaying the footer.
 *
 * @package Carpress
 * @since 1.0.0
 */
?>

	<div class="foot">
		<a href="#" id="to-the-top">
			<i class="fa  fa-chevron-up"></i>
		</a>
		<div class="container">

			<div class="row">

				<?php dynamic_sidebar( 'footer-sidebar-area' ); ?>

			</div><!-- /row -->
		</div><!-- /container -->
	</div><!-- /foot -->

	<footer>
		<div class="container">
			<div class="row">
				<div class="span12 text-center">
					<?php echo ot_get_option( 'footer_left', '&copy; Copyright 2013' ); ?>
				</div>
				<!--<div class="span6">
					<div class="pull-right"> -->
					<?php /* echo ot_get_option( 'footer_right', 'Carpress Theme by <a href="http://www.proteusthemes.com">ProteusThemes</a>' ); */ ?>
					<!-- </div>
				</div> -->
			</div>
		</div>
	</footer>
</div><!-- .boxed-container -->

<?php echo ot_get_option('footer_scripts', ''); ?>

<?php wp_footer(); ?>
<!-- W3TC-include-js-body-end -->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83674590-1', 'auto');
  ga('send', 'pageview');
  setTimeout("ga('send', 'event', 'read', '15_seconds')", 15000);

</script>




<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter39431410 = new Ya.Metrika({
                    id:39431410,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/39431410" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1773465856257464');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1773465856257464&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '2kUEU7PfIu';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
</html>