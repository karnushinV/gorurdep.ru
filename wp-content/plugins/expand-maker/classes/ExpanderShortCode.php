<?php

class ExpanderShortCode 
{
	public $args;

	public function setArgs($args)
	{
		$this->args = $args;
	}

	public function getArgs()
	{
		return $this->args;
	}

	public function __construct() {
		add_shortCode('expander_maker', array($this, 'doshorctode'));
	}

	public function doshorctode($args, $content) {
		$more = @$args['more'];
		$less = @$args['less'];

		$allData = DataProcessing::selectData();
		
		$width = $allData['width'];
		$height = $allData['height'];
		$duration = $allData['duration'];

		$args = $args = array(
			'more' => $more,
			'less' => $less,
			'width' => $width,
			'height' => $height,
			'duration' => $duration,
			'fontSize' => @$fontSize
		);
		$this->setArgs($args);
		$this->enqueueScripts();
		$sgexSontents = do_shortcode($content);
		return "<div class=\"expm-content\">$sgexSontents</div>
		<div class='expand-btn-wrappper'>
			<span class='expm-toggle-expand'>
				<span data-more='$more' data-less='$less' class=\"expm-button-text\">$more</span>
			</span>
		</div>";
	}

	public function enqueueScripts() {
		wp_register_script('expander-maker', EXPM_JAVASCRIPT.'expmaker.js', array('jquery'));
		wp_localize_script('expander-maker', 'expanderArgs', $this->getArgs());
		wp_enqueue_script('expander-maker');
		wp_enqueue_script('jquery');
		wp_register_style('expmaker_css', EXPM_CSS."expmaker.css");
		wp_enqueue_style('expmaker_css');
	}
}