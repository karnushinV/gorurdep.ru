<?php
require_once(CLASSES."ExpanderDataProcessing.php");
class ExpanderrIntaller
{
		public static function createTables($blogsId)
	{
		global $wpdb;
		$expanderDataBase = "CREATE TABLE IF NOT EXISTS ". $wpdb->prefix.$blogsId."expander_maker (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`width` varchar(255) NOT NULL,
			`height` varchar(255) NOT NULL,
			`duration` varchar(255) NOT NULL,
			`options` text NOT NULL,
			PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8; ";
		$wpdb->query($expanderDataBase);
	}

	public static function install()
	{
		$obj = new self();
		$obj->createTables("");
		$proessingObj = new DataProcessing;
		$proessingObj->deafultIntitialInsert("");
		if(is_multisite()) {
			$sites = wp_get_sites();
			foreach($sites as $site) {
				$blogsId = $site['blog_id']."_";
				global $wpdb;
				$obj->createTables($blogsId);
			}

			foreach($sites as $site) {
				$blogsId = $site['blog_id']."_";
				
				$proessingObj->deafultIntitialInsert($blogsId);
			}
		}
	}

	public static function uninstall()
	{
		$obj = new self();
		$obj->uninstallTables("");
		if (is_multisite()) {
			$sites = wp_get_sites();
			foreach($sites as $site) {
				$blogsId = $site['blog_id']."_";
				$obj->uninstallTables($blogsId);
			}
		}
	}

	public function uninstallTables($blogsId)
	{
		global $wpdb;
		$expanderTable = $wpdb->prefix."expander_maker";
		$expmSql = "DROP TABLE ". $expanderTable;
		var_dump($expmSql);
		die();
		$wpdb->query($expmSql);	
	}
}