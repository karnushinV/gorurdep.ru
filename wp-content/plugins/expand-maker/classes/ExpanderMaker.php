<?php
class ExpMaker
{
	function __construct()
	{
		require_once(CLASSES."ExpanderInstaller.php");
		require_once(CLASSES."ExpanderDataProcessing.php");
		$this->actions();
	}

	public function activate() {
		ExpanderrIntaller::install();
	}

	public function uninstall() {
		ExpanderrIntaller::uninstall();
	}

	public function shortcode()
	{
		require_once(CLASSES."ExpanderShortCode.php");
		$shortCodeObj = new ExpanderShortCode();
	}

	public function enqueueScripts()
	{
		
	}

	public function expmMeinMenu()
	{
		require_once(CLASSES."ExpanderPages.php");
		$obj = new ExpanderPages();
	}

	public function expmAdminMenu()
	{
		add_menu_page("Read More", "Read More", "manage_options","ExpMaker",array($this, 'expmMeinMenu'));
	}

	public function actions()
	{
		add_action("admin_menu", array($this, 'expmAdminMenu'));
		add_action('wp_head',  array($this, 'shortcode'));
		register_activation_hook(EXPM_PATH.'expand-maker.php',  array($this, 'activate'));
		//register_uninstall_hook(EXPM_PATH.'expand-maker.php',  array('ExpMaker', 'uninstall'));
		add_action('admin_post_update_data', array('DataProcessing', 'expanderUpdateData'));
	}
}