<?php

class DataProcessing 
{

	public function deafultIntitialInsert($blogsId) {
		global $wpdb;
		$options = array('font-size' => '14px');
		$buttonStyles = array(
			'width' => '100px',
			'height' => '32px',
			'duration' => '1000',
			'options' => json_encode($options)
		);
		$width = $buttonStyles['width'];
		$height = $buttonStyles['height'];
		$duration = $buttonStyles['duration'];
		$options = $buttonStyles['options'];
		$sql = $wpdb->prepare("INSERT INTO ".$wpdb->prefix.$blogsId."expander_maker (width, height, duration, options) VALUES (%s, %s, %s, %s)",$width,$height,$duration, $options);
		$res = $wpdb->query($sql);
	}

	public static function selectData() {
		global $wpdb;
		$st = $wpdb->prepare("SELECT width, height, duration, options FROM ".$wpdb->prefix."expander_maker WHERE id = %d", 1);
		$arr = $wpdb->get_row($st,ARRAY_A);
		return $arr;
	}

	public  function expanderUpdateData() {
		global $wpdb;
		$options = array(
			'font-size'=> $_POST['font-size']
			);
		$options = json_encode($options);
		$sql = $wpdb->prepare("UPDATE ".$wpdb->prefix."expander_maker SET 
			width=%s,
			height=%s,
			duration=%s,
			options=%s  WHERE id=%d", $_POST['button-width'], $_POST['button-height'],$_POST['animation-duration'], $options, 1);
		$res = $wpdb->query($sql);
		wp_redirect(admin_url()."admin.php?page=ExpMaker");
	}

	public static function localizeScriptData() {
		$datas = self::selectData();
		$width = $datas['width'];
		$height = $datas['height'];
		$duration = $datas['duration'];
		$options = json_decode($datas['options'], true);
		$fontSize = $options['font-size'];
		$args = array(
			'more' => "Read more",
			'less' => "Read Less",
			'width' => $width,
			'height' => $height,
			'duration' => $duration,
			'fontSize' => $fontSize
		);
		return $args;
	}
}