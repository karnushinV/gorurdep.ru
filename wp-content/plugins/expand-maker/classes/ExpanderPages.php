<?php
require_once(CLASSES."ExpanderDataProcessing.php");
class ExpanderPages
{
	public function __construct() {
		$data = DataProcessing::localizeScriptData();
		wp_register_script('expmaker', EXPM_JAVASCRIPT.'expmaker.js', array( 'wp-color-picker' ), array('jquery'));
		wp_localize_script('expmaker', 'expanderArgs', $data);
		wp_enqueue_script('expmaker');
		wp_enqueue_script('jquery');
		wp_register_style('expm-pages', EXPM_CSS . 'expmPages.css');
		wp_enqueue_style('expm-pages');
		wp_enqueue_style('wp-color-picker');
		$this->mainPage();
	}

	public function mainPage() {
		$allData = DataProcessing::selectData();
		require_once(EXPM_VIEWS."main.php");
	}

}