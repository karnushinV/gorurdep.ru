<?php 
	$options = json_decode($allData['options'], true);
	$fontSize = $options['font-size'];
?>
<form action="<?php echo admin_url();?>admin-post.php?action=update_data" method="post">
<div class="expm-wrapper">
	<div class="titles-wrapper">
		<h2 class="expander-page-title">Change settings</h2>
		<div class="button-wrapper">
			<p class="submit">
				<input type="button" class="expm-update-to-pro" value="Upgrade to PRO version" onclick="window.open('http://edmion.esy.es/')">
				<input type="submit" class="button-primary" value="<?php echo 'Save Changes'; ?>">
			</p>
		</div>
	</div>
	<div class="clear"></div>
	<div class="options-wrapper">
		<div id="expander-options">
			<div id="post-body" class="metabox-holder columns-2">
				<div id="postbox-container-2" class="postbox-container">
					<div id="normal-sortables" class="meta-box-sortables ui-sortable">
						<div class="postbox">
							<div class="handlediv" title="Click to toggle"><br></div>
							<h3 class="hndle ui-sortable-handle">
								<span>Expander Options</span>
							</h3>
							<div class="expander-options-content">
									<div class="expm-option-label">Button width(in pixels).</div>
									<input type="text" class="expm-options-margin expm-btn-width" name="button-width" value="<?php echo esc_attr($allData['width'])?>"><br>

									<div class="expm-option-label">Button height(in pixels).</div>
									<input type="text" class="expm-options-margin expm-btn-height" name="button-height" value="<?php echo esc_attr($allData['height'])?>"><br>

									<div class="expm-option-label">Font size(in pixels).</div>
									<input type='text' class="expm-option-font-size" name="font-size" value="<?php echo esc_attr($fontSize)?>">
									
									<div class="expm-option-label">Set animation speed.</div>
									<input type="text" class="expm-options-margin" name="animation-duration" value="<?php echo esc_attr($allData['duration'])?>">

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</form>
		<div class="clear"></div>
		<div id="expander-pro-option" onclick="window.open('http://edmion.esy.es/')">
			<div id="post-body" class="metabox-holder columns-2">
				<div id="postbox-container-2" class="postbox-container">
					<div id="normal-sortables" class="meta-box-sortables ui-sortable">
						<div class="postbox">
							<div class="handlediv" title="Click to toggle"><br></div>
							<h3 class="hndle ui-sortable-handle">
								<span>Advanced options</span>
							</h3>
							<div class="expander-options-content">

								<div class="expm-option-label">Background Color.</div>
								<input type="text" class="background-color expm-margin-left" name="btn-background-color" value="">
								
								<div class="expm-option-label">Text Color.</div>
								<input type="text" class="background-color expm-margin-left" name="btn-text-color" value="">
								
								<div class="expm-option-label">Font Family.</div>
								<select><option>Chewy</option></select>
								<div class="expm-option-label">Border readius.</div>
								<input type="text" class="btn-border-radius expm-margin-left" name="btn-border-radius" value="">
								
								<div class="expm-option-label">Horizontal alignment</div>
								<select class="expm-margin-left">
									<option>Center</option>
								</select>
								
								<div class="expm-option-label">Vertical alignment</div>
								<select class="expm-margin-left">
									<option>Bottom</option>
								</select>

								<div class="expm-option-label">Show olny mobile devices</div>
									<input type="checkbox" name="show-only-mobile" <?php echo $showOlnyMobileDevices;?>>

								<script type="text/javascript">
									jQuery(document).ready(function() {
										jQuery(".background-color").each(function() {jQuery(this).wpColorPicker() });
									});
										
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="preview-wrapper">
		<h1 class="live-hedline">Live Preview</h1>

		<div class="expm-content">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
		</div>
		<div class='expand-btn-wrappper'>
		<span class='expm-toggle-expand'><span class="expm-button-text">Read more</span></span></div>
	</div>
	<input type="text" id="expm-shortcode-info-div" class="widefat" readonly="readonly" value='[expander_maker more="Read more" less="Read less"]Expander hidden text[/expander_maker]'>
</div>

