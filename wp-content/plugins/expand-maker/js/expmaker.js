function EXPmaker()
{

}

EXPmaker.prototype.binding = function() {
	var thaT = this;
	var duration = parseInt(expanderArgs.duration);
	
	jQuery('.expm-toggle-expand').toggle(function() {
		jQuery(this).parent().prev().slideDown(duration);
		var lessName = jQuery(this).find(".expm-button-text").attr('data-less');
		jQuery(this).find(".expm-button-text").text(lessName);
	}, function() {
		var moreName = jQuery(this).find(".expm-button-text").attr('data-more');
		jQuery(this).parent().prev().slideUp(duration);
		jQuery(this).find(".expm-button-text").text(moreName);
	});

	this.buttonDimentions();
	this.styles();
}

EXPmaker.prototype.buttonDimentions = function() {
	var width = expanderArgs.width;
	var height = expanderArgs.height;
	
	jQuery(".expm-toggle-expand").css({
		'width': width,
		'height': height
	});
}

EXPmaker.prototype.styles = function() {
	var fontSize = expanderArgs.fontSize;
	jQuery(".expm-button-text").css({
		'font-size': fontSize
	})
}

EXPmaker.prototype.livePreview = function() {
	this.changeButtonWidth();
	this.changeButtonHeight();
	this.changeButtonFontSize();
}

EXPmaker.prototype.changeButtonWidth = function() {
	jQuery('.expm-btn-width').change(function() {
		var width = jQuery(this).val();
		jQuery(".expm-toggle-expand").css({
			"width": width
		});
	});
}

EXPmaker.prototype.changeButtonHeight = function() {
	jQuery(".expm-btn-height").change(function() {
		var height = jQuery(this).val();
		jQuery(".expm-toggle-expand").css({
			"height": height
		});
	});
}

EXPmaker.prototype.changeButtonFontSize = function() {
	jQuery('.expm-option-font-size').change(function() {
		var size = jQuery(this).val();
		jQuery(".expm-button-text").css({
			'font-size': size
		})
	});
}
/*init*/
jQuery(document).ready(function() {
	expObj = new EXPmaker();
	expObj.binding();
	expObj.livePreview();
});