<?php
define("EXPM_PATH", dirname(__FILE__).'/');
define("CLASSES", EXPM_PATH."classes/");
define('EXPM_URL', plugins_url('', __FILE__).'/');
define("EXPM_JAVASCRIPT", EXPM_URL."js/");
define("EXPM_CSS", EXPM_URL."css/");
define("EXPM_VIEWS", EXPM_PATH."views/");
