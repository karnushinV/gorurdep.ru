=== Read More ===
Contributors: Parker
Tags: Tags: read more ,expander, expand, expandable,read less ,Accordion,show more,Do more,WP show more, Expanding, collapsible, display, expandable content,  show more, hidden, hide, javascript, jquery, more, read me,  shortcode
Requires at least: 3.8
Tested up to: 4.7.2
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The best wordpress "Read more" plugin to help you show or hide your long content.

== Description ==
Read More is the best wordpress **Read More** plugin to help you show or hide your long content.

With the help of **Read More** plugin you can make website with long content more beautiful. The part of the page you don`t want to see immediately,will be hidden after inserting it in the short code and will open by clicking the button.
Use it to toggle (show/hide) blocks of text, by inserting a simple short code:

** Read more - Features: **

* Read more button width - Read more button custom dimension
* Read more button height - Read more button custom dimension
* Read more Font size - Read more button custom font size
* Expand animation duration - Expand custom animation duration

** Read more - PRO features: **

* Read more button background color - Read more button custom color
* Read more button font family - Read more button font family from google fonts
* Read more button color - Read more button text custom color 
* Read more button border radius - Read more button border radius 
* Read more button horizontal alignment - Read more button horizontal alignment
* Read more button vertical alignment - Read more button vertical alignment
* Read more button vertical alignment - Read more button vertical alignment
* Show Read more only on mobile devices - after activating this option, Read more will be shown only on mobile devices.

<a href="http://edmion.esy.es/" target="_blank" >Get Read More PRO package</a>

Shortcode example: 
[expander_maker more="Read more" less="Read less"]Hidden text[/expander_maker]


If you think that you found a bug in Expander Maker plugin or have any questions, please feel free to contact us at <b>edmon.parker@gmail.com</b>.
 
== Changelog ==
= 1.1.8 =
* Bug fixed
* code improvement

= 1.1.7 =
* multiple values of “more” and “less” examples 
[expander_maker more="Read more" less="Read less"]Hidden text[/expander_maker]
[expander_maker more="Read more1" less="Read less1"]Hidden text[/expander_maker]
* Bug fixed

= 1.1.6 =
* Bug fixed
* Change Font Family (Pro)
* Show only on mobile devices (Pro)

= 1.0.1 =
* Fixed button style.

= 1.0.2 = 
* Bug fixed.
* Changed shortcode name
* Fixed button style.

= 1.0.3 = 
* Bug fixed.
* Button width option.
* Button height option.
* Animation duration option.
* Fixed button style.

= 1.1.3 = 
* Bug fixed.
* Font size.
* Pro version.
* Admin view change.

= 1.1.5 = 
* Bug fixed.

== Frequently Asked Questions ==

Comming soon.

== Screenshots ==
1.  Read more button position 1
2.  Read more button position 2
3.  Read more button position 3
4. Read more button position 4